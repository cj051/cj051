#include<stdio.h>
int main()
{
	int num1,num2,temp=0,*pnum1,*pnum2,*ptemp;
	pnum1=&num1;
	pnum2=&num2;
	ptemp=&temp;
	printf("\n Enter the first number");
	scanf("%d",pnum1);
	printf("\n Enter the second number");
	scanf("%d",pnum2);
	*ptemp=*pnum1;
	*pnum1=*pnum2;
	*pnum2=*ptemp;
	printf("\n The first number now is %d",*pnum1);
	printf("\n The second number now is %d",*pnum2);
	return 0;
}