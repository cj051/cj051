#include <stdio.h>
int main()
{
    char Str[75];
    int i=0,j,length=0;
    printf("\n Enter the string to be checked :");
    gets(Str);
    while(Str[i]!='\0')
    {
        length++;
        i++;
    }
    i=0;
    j=length-1;
    while(i<=length/2)
    {
        if(Str[i]==Str[j])
        {
            i++;
            j--;
        }
        else
        {
            break;
        }
    }
    if(i>=j)
    {
        printf("\n The entered string is a Palindrome");
    }
    else
    {
        printf("\n The entered string is not a Palindrome ");
    }
    return 0;
 }
