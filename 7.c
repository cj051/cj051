#include<stdio.h>
int main()
{
 int i,j,Mat[3][3],transpose_Mat[3][3];
 printf("\n Enter the elements of the matrice");
 for(i=0;i<3;i++)
 {
  for(j=0;j<3;j++)
  {
   scanf("%d",&Mat[i][j]);
  }
 }

 printf("\n The entered matrice is");
 for(i=0;i<3;i++)
 {
  printf("\n");
  for(j=0;j<3;j++)
  {
   printf(" \t %d",Mat[i][j]);
  }
 }

 for(i=0;i<3;i++)
 {
  for(j=0;j<3;j++)
  {
   transpose_Mat[i][j]=Mat[j][i];
  }
 }

 printf("\n ****************************************");
 printf("\n The Transposed matrice is ");
 for(i=0;i<3;i++)
 {
  printf("\n");
  for(j=0;j<3;j++)
  {
   printf(" \t %d",transpose_Mat[i][j]);
  }
 }
 return 0;
}
