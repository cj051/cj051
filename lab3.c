#include<stdio.h>
#include<math.h>
int main()
{
	int a,b,c;
	float D,deno,root1,root2;
	printf("\n Enter the coefficients of the quadratic equation:");
	scanf("%d %d %d",&a,&b,&c);
	deno=2.0*a;
	D=(b*b)-(4*a*c);
	if(D>0.0)
	{
		printf("\n REAL ROOTS");
		root1=(-b+sqrt(D))/deno;
		root2=(-b-sqrt(D))/deno;
		printf("\n ROOT1 =%f \t ROOT2 =%f",root1,root2);
	}
	else if(D==0.0)
	{
		printf("\n EQUAL ROOTS");
		root1=-b /deno;
		printf("\n ROOT1 =%f \t ROOT2=%f",root1,root1);
	}
	else
	{
		printf("\n IMAGINARY ROOTS");
	}
	return 0;
}