#include<stdio.h>
int main()
{
    int num1,num2,sum=0,mult=0,sub=0,rem=0,div=0;    
    int *pnum1,*pnum2,*psum,*pmult,*prem,*psub,*pdiv;    
    pnum1=&num1;
    pnum2=&num2;
    psum=&sum;
    pmult=&mult;
    psub=&sub;
    prem=&rem;
    pdiv=&div;
    printf("\n Enter the first number ");
    scanf("%d",pnum1);
    printf("\n Enter the second number ");
    scanf("%d",pnum2);
    *psum = *pnum1 + *pnum2;
    *pmult = (*pnum1)*(*pnum2);
    *pdiv = (*pnum1)/(*pnum2);
    *psub = *pnum1 - *pnum2;
    *prem = (*pnum1)%(*pnum2);
    printf("\n The result of addition of two numbers is = %d",*psum);
    printf("\n The result of subtraction of two numbers is = %d",*psub);
    printf("\n The result of multiplication of two numbers is = %d",*pmult);
    printf("\n The result of division of two numbers is = %d",*pdiv);
    printf("\n The result of the remainder of two numbers is = %d",*prem);
    return 0;
}


